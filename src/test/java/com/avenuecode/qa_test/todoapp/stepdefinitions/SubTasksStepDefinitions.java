package com.avenuecode.qa_test.todoapp.stepdefinitions;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.openqa.selenium.WebDriver;

import com.avenuecode.qa_test.todoapp.pages.SubtasksPage;
import com.avenuecode.qa_test.todoapp.utils.DataUtils;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SubTasksStepDefinitions {

	private BaseStepDefinitions context;
	private WebDriver driver;
	private SubtasksPage subtasksPage;
	private String descricaoSubtask;

	public SubTasksStepDefinitions(BaseStepDefinitions context) {
		this.context = context;
		driver = this.context.driver;
		subtasksPage = new SubtasksPage(driver);
	}

	@Then("o sistema deve exibir o modal de subtasks com apresentar o ID e descrição da task desabilitados")
	public void oSistemaDeveEexibirOModalDeSubtasksEApresentarOIdEDescricaoDaTaskDesabilitados() {
		assertThat("Modal não foi exibido", subtasksPage.modalExibido(), equalTo(true));
		assertThat("Descrição da Task habilitada", subtasksPage.modalExibido(), equalTo(false));
	}

	@Then("o formulário para inclusão de subtask com os campos de descrição e data")
	public void oFormularioParaInclusaoDeSubtaskComOsCamposDeDescricaoEData() {
		assertThat("Campo descrição da subtask não foi exibido", subtasksPage.descricaoSubtaskExibido(), equalTo(true));
		assertThat("Campo due date não foi exbido", subtasksPage.dueDateExibido(), equalTo(true));
	}

	@Then("o botão para adicionar subtasks")
	public void oBotaoParaAdicionarSubtasks() {
		assertThat("Botão add subtask não foi exbido", subtasksPage.addSubtaskExibido(), equalTo(true));
	}

	@Then("a lista de subtasks adcionadas na parte inferior")
	public void aListaDeSubtasksAdcionadasNaParteInferior() {
		subtasksPage.preencherDescricaoSubtask("Teste");
		subtasksPage.clicarEmAddSubtask();
		assertThat("Tabela com subtasks não foi exbida", subtasksPage.tabelaSubtasksExibida(), equalTo(true));
	}

	@When("eu informar a descrição e a data da subtask")
	public void euInformarADescricaoEADataDaSubtask() {
		descricaoSubtask = "Subtask " + DataUtils.dataAtualFormatada("dd-MM-yyyy HH:mm:ss");
		subtasksPage.preencherDescricaoSubtask(descricaoSubtask);
		subtasksPage.preencherDueDate(DataUtils.dataAtualFormatada("MM/dd/yyyy"));
	}

	@When("acionar a opção para adicionar subtask")
	public void acionarAOpcaoParaAdicionarSubtask() {
		subtasksPage.clicarEmAddSubtask();
	}

	@Then("o sistema deve adicionar a subtask à lista de subtasks")
	public void oSistemaDeveAdicionarASubtaskAListaDeSubtasks() {
		assertThat(String.format("Subtask '%s' não foi adicionada à lista", descricaoSubtask),
				subtasksPage.subtaskAdicionadaLista(descricaoSubtask), equalTo(true));
	}

	@When("eu informar os dados da subtask sem informar o campo {string}")
	public void euInformarOsDadosDaSubtaskSemInformarOCampo(String campo) {
		if ("descrição".equals(campo)) {
			subtasksPage.limparDescricao();
			subtasksPage.preencherDueDate(DataUtils.dataAtualFormatada("MM/dd/yyyy"));
		} else if ("data".equals(campo)) {
			descricaoSubtask = "Subtask " + DataUtils.dataAtualFormatada("dd-MM-yyyy HH:mm:ss");
			subtasksPage.preencherDescricaoSubtask(descricaoSubtask);
			subtasksPage.limparDueDate();
		}
	}

	@Then("o sistema não deve adicionar a subtask à lista de subtasks")
	public void oSistemaNaoDeveAdicionarASubtaskAListaDeSubtasks() {
		assertThat("Subtask adicionada à lista", subtasksPage.subtasksAdicionadas(), equalTo(0));
	}

	@Then("deve informar que o campo {string} é de preenchimento obrigatório")
	public void deveInformarQueOCampoEDePreenchimentoObrigatorio(String string) {
		throw new PendingException();
	}

	@When("eu preencher o formulário para inclusão de subtask com o campo de descrição com mais de {int} caracteres e a data")
	public void oformularioParaInclusaoDeSubtaskComOCampoDeDescricaoComMaisDeCaracteresEAData(Integer maximoCaracteres) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i <= maximoCaracteres; i++) {
			str.append("a");
		}
		descricaoSubtask = str.toString();
		subtasksPage.preencherDescricaoSubtask(descricaoSubtask);
		subtasksPage.preencherDueDate(DataUtils.dataAtualFormatada("MM/dd/yyyy"));
	}

	@Then("deve informar que a descrição deve possuir no máximo {int} caracteres")
	public void deveInformarQueADescricaoDevePossuirNoMaximoCaracteres(Integer int1) {
		throw new PendingException();
	}

	@When("eu preencher o formulário para inclusão de subtask com o campo de descrição e a data {string}")
	public void oFormularioParaInclusaoDeSubtaskComOCampoDeDescricaoEAData(String data) {
		descricaoSubtask = "Subtask " + DataUtils.dataAtualFormatada("dd-MM-yyyy HH:mm:ss");
		subtasksPage.preencherDescricaoSubtask(descricaoSubtask);
		subtasksPage.preencherDueDate(data);
	}

	@Then("deve informar que o formato da data deve ser {string}")
	public void deveInformarQueOFormatoDaDataDeveSer(String string) {
		throw new PendingException();
	}

	@Given("exista subtasks para a task")
	public void existaSubtasksParaATask() {
		descricaoSubtask = "Subtask " + DataUtils.dataAtualFormatada("dd-MM-yyyy HH:mm:ss");
		subtasksPage.preencherDescricaoSubtask(descricaoSubtask);
		subtasksPage.preencherDueDate(DataUtils.dataAtualFormatada("MM/dd/yyyy"));
		subtasksPage.clicarEmAddSubtask();
	}

	@When("eu acionar a opção para remover subtask")
	public void euAcionarAOpcaoParaRemoverSubtask() {
		subtasksPage.clicarEmRemoveSubtask(descricaoSubtask);
	}

	@Then("o sistema deve remover a subtask da lista de subtasks")
	public void oSistemaDeveRemoverASubtaskDaListaDeSubtasks() {
		assertThat(String.format("Subtask '%s' não foi removida da lista", descricaoSubtask),
				subtasksPage.subtaskAdicionadaLista(descricaoSubtask), equalTo(false));
	}

}
