package com.avenuecode.qa_test.todoapp.stepdefinitions;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.openqa.selenium.WebDriver;

import com.avenuecode.qa_test.todoapp.pages.HomePage;
import com.avenuecode.qa_test.todoapp.pages.TasksPage;
import com.avenuecode.qa_test.todoapp.utils.DataUtils;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TasksStepDefinitions {

	private static final String USUARIO_LOGADO = "Victor Guerra";

	private String descricaoTask;

	private BaseStepDefinitions context;
	private WebDriver driver;
	private HomePage homePage;
	private TasksPage tasksPage;

	public TasksStepDefinitions(BaseStepDefinitions context) {
		this.context = context;
		driver = this.context.driver;
		homePage = new HomePage(driver);
		tasksPage = new TasksPage(driver);
	}

	@Given("^acessar o link para a página 'My Tasks'|tenha acessado a página 'MyTasks'$")
	public void acessarOLinkParaAPaginaMyTasks() {
		homePage.clicarEmMyTasks();
	}

	@Then("o sistema deve apresentar a lista de tasks do usuário")
	public void oSistemaDeveApresentarAListaDeTasksDoUsuario() {
		assertThat("Lista com as Tasks não foi exibida", tasksPage.tabelaTasksExibida(), equalTo(true));
	}

	@Then("apresentar a mensagem {string}")
	public void apresentarAMensagem(String mensagem) {
		assertThat("Mensagem incorreta", tasksPage.mensagemUsuario(),
				equalTo(mensagem.replace("<nome do usuario>", USUARIO_LOGADO)));
	}

	@When("eu informar na descrição da task {string}")
	public void euInformarNaDescricaoDaTask(String task) {
		if (task.contains("<texto com mais de 250 caracteres>")) {
			StringBuilder str = new StringBuilder();
			for (int i = 0; i < 251; i++) {
				str.append("a");
			}
			descricaoTask = str.toString();
		} else if (task.contains("<data e hora atual>"))
			descricaoTask = task.replace("<data e hora atual>", DataUtils.dataAtualFormatada("dd-MM-yyyy HH:mm:ss"));
		else
			descricaoTask = task;
		tasksPage.preencherDescricaoTask(descricaoTask);
	}

	@When("pressionar Enter")
	public void pressionarEnter() {
		tasksPage.pressionarEnterDescricaoTask();
	}

	@When("acionar a opção para adcionar a Task")
	public void acionarAOpcaoParaAdcionarATask() {
		tasksPage.clicarEmAddTask();
	}

	@Then("o sistema deve adicionar a task informada à lista de tasks")
	public void oSistemaDeveAdicionarATaskInformadaAListaDeTasks() {
		assertThat("Task não foi adicionada à lista", tasksPage.taskAdicionadaLista(descricaoTask), equalTo(true));
	}

	@Then("^o sistema não deve permitir a inclusão da task|o sistema deve remover a task informada da lista de tasks$")
	public void oSistemaNaoDevePermitirAInclusaoDaTask() {
		assertThat(String.format("A task '%s' ainda é exibida na lista", descricaoTask), tasksPage.taskAdicionadaLista(descricaoTask), equalTo(false));
	}

	@Then("informar os tamanhos mínimo e máximo para a descrição da task")
	public void informarOsTamanhosMinimoEMaximoParaADescricaoDaTask() {
		throw new PendingException();
	}
	
	@Given("exista tasks exibidas na lista")
	public void existaTasksExibidasNaLista() {
		descricaoTask = "Task " + DataUtils.dataAtualFormatada("dd-MM-yyyy HH:mm:ss");
		tasksPage.preencherDescricaoTask(descricaoTask);
		tasksPage.clicarEmAddTask();
	}
	
	@When("eu aciono a opção para remover a task")
	public void euAcionoAOpcaoParaRemoverATask() {
		tasksPage.clicarEmRemove(descricaoTask);
	}
	
	@When("eu acionar a opção para gerenciar subtasks")
	public void euAcionarAOpcaoParaGerenciarSubtasks() {
		tasksPage.clicarEmManageSubtasks(descricaoTask);
	}
}
