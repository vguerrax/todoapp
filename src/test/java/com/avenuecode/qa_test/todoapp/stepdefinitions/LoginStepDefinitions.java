package com.avenuecode.qa_test.todoapp.stepdefinitions;

import org.openqa.selenium.WebDriver;

import com.avenuecode.qa_test.todoapp.pages.LoginPage;

import cucumber.api.java.en.Given;

public class LoginStepDefinitions {

	private static final String EMAIL = "victor.guerra_@hotmail.com";
	private static final String SENHA = "teste123";
	
	private BaseStepDefinitions context;
	private  WebDriver driver;
	private LoginPage loginPage;
	
	public LoginStepDefinitions(BaseStepDefinitions context) {
		this.context = context;
		driver = this.context.driver;
		loginPage = new LoginPage(driver);
	}
	
	@Given("eu tenha acessado a aplicação como usuário padrão")
	public void euTenhaAcessadoAAplicacaoComoUsuarioPadrao() {
		loginPage.acessarPaginaInicial();
		loginPage.clicarEmSingInHome();
		loginPage.preencherEmail(EMAIL);
		loginPage.preencherSenha(SENHA);
		loginPage.clicarEmSigIn();
	}
}
