package com.avenuecode.qa_test.todoapp.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DataUtils {

	public static String dataAtualFormatada(String formato) {
        SimpleDateFormat format = new SimpleDateFormat(formato); 
        return format.format(new Date());
    }
}
