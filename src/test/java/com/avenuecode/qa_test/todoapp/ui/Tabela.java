package com.avenuecode.qa_test.todoapp.ui;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.avenuecode.qa_test.todoapp.pages.BasePage;

/**
 * <p>
 * Tabela class.
 * </p>
 *
 * @author Victor Guerra
 * @version $Id: $Id
 * @since 1.0.0
 */
public class Tabela extends BasePage {
	private WebDriverWait wait;
	
	private static final String XPATH_CABECALHO = "./thead/tr[###]/th";

	private static final String XPATH_LINHA = "./tbody/tr";

	private static final String XPATH_CELULA = "./td";

	private WebElement webElementoTabela;
	
	private int posicaoCabecalho = 0;
	
	private boolean linhasOcultas = false;

	/**
	 * <p>
	 * Constructor for Tabela.
	 * </p>
	 *
	 * @param driver
	 *            a {@link org.openqa.selenium.WebDriver} object.
	 * @param tabela
	 *            a {@link org.openqa.selenium.By} object.
	 */
	public Tabela(WebDriver driver, By tabela) {
		super(driver);
		this.wait = new WebDriverWait(driver, 30);
		this.webElementoTabela = wait.until(ExpectedConditions.presenceOfElementLocated(tabela));
	}
	
	public Tabela(WebDriver driver, WebElement tabela) {
		super(driver);
		this.wait = new WebDriverWait(driver, 30);
		this.webElementoTabela = wait.until(ExpectedConditions.visibilityOf(tabela));
	}
	
	
	/**
	 * Define a posição do cabeçalho para tabelas com cabeçalho múltiplo
	 * 
	 * @param posicao 
	 *                 Posição da linha de cabeçalho, indexada em 0
	 *                  
	 */
	public void definirPosicaoCabecalho(int posicao) {
		posicaoCabecalho = posicao;
	}

	/**
	 * Busca uma linha na tabela.
	 *
	 * @param map
	 *            parâmetros mapeamento com o cabeçalho e texto de busca
	 * @return linha na tabela
	 * @throws br.com.unimedbh.qacommons.core.exceptions.UnimedTestException
	 *             se nenhuma linha for encontrada
	 */
	public WebElement buscarLinha(Map<String, String> map) {
		for (WebElement linha : linhas()) {
			if (contemParametrosNaLinha(map, linha)) {
				return linha;
			}
		}
		return null;
	}

	/**
	 * Busca uma linha na tabela.
	 *
	 * @param map
	 *            mapeamento com o cabeçalho e texto de busca
	 * @param indice
	 *            posição da celula desejada na linha.
	 * @return célula na linha da tabela
	 * @throws br.com.unimedbh.qacommons.core.exceptions.UnimedTestException
	 *             se nenhuma linha for encontrada
	 */
	public WebElement buscarLinha(Map<String, String> map, int indice) {
		WebElement linha = buscarLinha(map);
		List<WebElement> celulas = celulas(linha);
		return celulas.get(indice);
	}

	/**
	 * Busca um elemento dentro de uma célula na tabela.
	 *
	 * @param map
	 *            mapeamento com o cabeçalho e texto de busca
	 * @param indice
	 *            posição da celula desejada na linha
	 * @param localizador
	 *            localizador do subelemento
	 * @return célula na linha da tabela
	 * @throws br.com.unimedbh.qacommons.core.exceptions.UnimedTestException
	 *             se nenhuma linha for encontrada
	 */
	public WebElement buscarLinha(Map<String, String> map, int indice, By localizador) {
		WebElement celula = buscarLinha(map, indice);
		return wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(celula, localizador));
	}

	/**
	 * Busca uma linha na tabela.
	 * 
	 * @param map
	 * @param linha
	 * @return verdadeiro se a linha na tabela possui os parâmetros informados @
	 */
	private boolean contemParametrosNaLinha(Map<String, String> map, WebElement linha) {
		boolean resultado = true;
		for (Entry<String, String> entrada : map.entrySet()) {
			resultado = resultado && contemTexto(linha, entrada.getKey(), entrada.getValue());
		}
		return resultado;
	}

	/**
	 * Retorna se existe o texto em uma celula da linha.
	 * 
	 * @param linha
	 * @param cabecalho
	 * @param texto
	 * @return verdadeiro se a celula possui o texto @
	 */
	private boolean contemTexto(WebElement linha, String cabecalho, String texto) {
		WebElement celula = celula(linha, cabecalho);
		String textoElemento = celula.getText().trim().toLowerCase();
		return textoElemento.trim().equalsIgnoreCase(texto);
	}

	/**
	 * Retorna o índice na tabela de acordo com o cabeçalho.
	 * 
	 * @param cabecalho
	 * @return de 0 ao total de colunas caso encontre o cabeçalho; @
	 */
	private int indice(String cabecalho) {
		List<WebElement> cabecalhos = cabecalho();
		for (int i = 0; i < cabecalhos.size(); i++) {
			if (cabecalhos.get(i).getText().trim().equalsIgnoreCase(cabecalho)) {
				return i;
			}
		}
		String messagem = String.format("Não foi encontrado nenhum cabeçalho na tabela com o texto: '%s'.", cabecalho);
		throw new NoSuchElementException(messagem);
	}

	/**
	 * @return lista de elementos contendo o cabecalho
	 */
	private List<WebElement> cabecalho() {
		By localizador = By.xpath(XPATH_CABECALHO.replace("###", String.valueOf(posicaoCabecalho+1)));
		return webElementoTabela.findElements(localizador);
	}

	/**
	 * Retorna as linhas de uma tabela.
	 * 
	 * @return lista com as linhas da tabela.
	 */
	private List<WebElement> linhas() {
		By localizador = By.xpath(XPATH_LINHA);
		List<WebElement> linhas = webElementoTabela.findElements(localizador);
		
		if(linhasOcultas)
			return linhas;
		return linhasVisiveis(linhas);
	}
	
	/**
	 * Retorna as linhas visiveis de uma lista de linhas
	 * @param linhas lista com as linhas encontradas
	 * @return lista com as linhas visíveis da tabela. 
	 */
	private List<WebElement> linhasVisiveis(List<WebElement> linhas) {
		List<WebElement> linhasVisiveis = new ArrayList<>();
		for(WebElement linha : linhas) {
			if(linha.isDisplayed())
				linhasVisiveis.add(linha);
		}
		return linhasVisiveis;
	}

	/**
	 * Retorna uma lista da tabela separada por células.
	 * 
	 * @return lista com as linhas da tabela separada por células.
	 */
	public List<List<WebElement>> linhasComCelulas() {
		List<List<WebElement>> lista = new LinkedList<>();
		for (WebElement linha : linhas()) {
			lista.add(celulas(linha));
		}
		return lista;
	}

	/**
	 * Retorna as células de uma linha na tabela.
	 * 
	 * @param linha
	 * @return celulas de uma linha.
	 */
	private List<WebElement> celulas(WebElement linha) {
		By localizador = By.xpath(XPATH_CELULA);
		return linha.findElements(localizador);
	}

	/**
	 * Retorna uma célula de acordo com o cabeçalho.
	 * 
	 * @param linha
	 * @param cabecalho
	 * @return célula 
	 */
	private WebElement celula(WebElement linha, String cabecalho) {
		int indice = indice(cabecalho);
		List<WebElement> celulas = celulas(linha);
		return celulas.get(indice);
	}
}
