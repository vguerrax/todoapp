package com.avenuecode.qa_test.todoapp.tests;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/MyTasks/SubTasks.feature", glue = {
		"classpath:com.avenuecode.qa_test.todoapp.stepdefinitions" })
public class SubTasksTest {

}
