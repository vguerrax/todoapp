package com.avenuecode.qa_test.todoapp.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {
	
	private By emailField = By.id("user_email");
	
	private By senhaField = By.id("user_password");
	
	private By singInButton = By.cssSelector("input[type='submit']");

	public LoginPage(WebDriver driver) {
		super(driver);
	}
	
	public void preencherEmail(String email) {
		clearAndSendKeys(emailField, email);
	}
	
	public void preencherSenha(String senha) {
		clearAndSendKeys(senhaField, senha);
	}
	
	public void clicarEmSigIn() {
		click(singInButton);
	}
}