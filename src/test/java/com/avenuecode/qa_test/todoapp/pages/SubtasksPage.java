package com.avenuecode.qa_test.todoapp.pages;

import java.util.LinkedHashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.avenuecode.qa_test.todoapp.ui.Tabela;

public class SubtasksPage extends BasePage {

	private By modalSubtasks = By.xpath("//*[@class='modal-content']");

	private By descricaoTaskField = By.id("edit_task");

	private By descricaoSubtaskField = By.id("new_sub_task");

	private By dueDateField = By.id("dueDate");

	private By addSubtaskButton = By.id("add-subtask");

	private By subtasksTable = By.cssSelector("div.modal-dialog table");

	private By removeSubtaskButton = By.xpath("//*[@ng-click='removeSubTask(sub_task)']");

	public SubtasksPage(WebDriver driver) {
		super(driver);
	}

	public boolean modalExibido() {
		return returnIfElementIsDisplayed(modalSubtasks);
	}

	public boolean descricaoTaskHabilitado() {
		return returnIfElementIsEnabled(descricaoTaskField);
	}

	public boolean descricaoSubtaskExibido() {
		return returnIfElementIsDisplayed(descricaoSubtaskField);
	}

	public boolean dueDateExibido() {
		return returnIfElementIsDisplayed(dueDateField);
	}
	
	public boolean addSubtaskExibido() {
		return returnIfElementIsDisplayed(addSubtaskButton);
	}
	
	public boolean tabelaSubtasksExibida() {
		return returnIfElementIsDisplayed(subtasksTable);
	}

	public void preencherDescricaoSubtask(String descricao) {
		sendKeys(descricaoSubtaskField, descricao);
	}

	public void preencherDueDate(String data) {
		clearAndSendKeys(dueDateField, data);
	}

	public void clicarEmAddSubtask() {
		click(addSubtaskButton);
	}
	
	public void limparDescricao() {
		clear(descricaoSubtaskField);
	}
	
	public void limparDueDate() {
		clear(dueDateField);
	}
	
	public int subtasksAdicionadas() {
		Tabela tabela = new Tabela(driver, subtasksTable);
		return tabela.linhasComCelulas().size();
	}
	
	public boolean subtaskAdicionadaLista(String subtRask) {
		Tabela tabela = new Tabela(driver, subtasksTable);
		Map<String, String> mapLinha = new LinkedHashMap<>();
		mapLinha.put("SubTask", subtRask);
		return tabela.buscarLinha(mapLinha) != null;
	}
	
	public void clicarEmRemoveSubtask(String subtRask) {
		Tabela tabela = new Tabela(driver, subtasksTable);
		Map<String, String> mapLinha = new LinkedHashMap<>();
		mapLinha.put("SubTask", subtRask);
		tabela.buscarLinha(mapLinha, 2, removeSubtaskButton).click();
	}
}