package com.avenuecode.qa_test.todoapp.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {

	private By myTasksLink = By.id("my_task");
	
	public HomePage(WebDriver driver) {
		super(driver);
	}
	
	public void clicarEmMyTasks() {
		click(myTasksLink);
	}
}
