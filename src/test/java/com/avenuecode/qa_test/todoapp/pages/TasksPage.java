package com.avenuecode.qa_test.todoapp.pages;

import java.util.LinkedHashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import com.avenuecode.qa_test.todoapp.ui.Tabela;

public class TasksPage extends BasePage {

	private By tasksTable = By.cssSelector("div.panel.panel-default table");
	
	private By mensagemUsuarioLabel = By.xpath("//h1");
	
	private By descricaoField = By.id("new_task");
	
	private By addTaskButton = By.xpath("//*[@ng-click='addTask()']");
	
	private By manageSubtasksButton = By.xpath(".//*[@ng-click='editModal(task)']");
	
	private By removeButton = By.xpath(".//*[@ng-click='removeTask(task)']");
	
	private By concluidaCheck = By.xpath(".//*[@type='checkbox']");
	
	public TasksPage(WebDriver driver) {
		super(driver);
	}
	
	public void preencherDescricaoTask(String descricaoTask) {
		sendKeys(descricaoField, descricaoTask);
	}
	
	public void pressionarEnterDescricaoTask() {
		sendKeys(descricaoField, Keys.ENTER);
	}
	
	public void clicarEmAddTask() {
		click(addTaskButton);
	}

	public boolean tabelaTasksExibida() {
		return returnIfElementIsDisplayed(tasksTable);
	}
	
	public String mensagemUsuario() {
		return getText(mensagemUsuarioLabel);
	}
	
	public boolean taskAdicionadaLista(String task) {
		Tabela tabela = new Tabela(driver, tasksTable);
		Map<String, String> mapLinha = new LinkedHashMap<>();
		mapLinha.put("Todo", task);
		return tabela.buscarLinha(mapLinha) != null;
	}
	
	public void clicarEmManageSubtasks(String task) {
		Tabela tabela = new Tabela(driver, tasksTable);
		Map<String, String> mapLinha = new LinkedHashMap<>();
		mapLinha.put("Todo", task);
		tabela.buscarLinha(mapLinha, 3, manageSubtasksButton).click();
	}
	
	public void clicarEmRemove(String task) {
		Tabela tabela = new Tabela(driver, tasksTable);
		Map<String, String> mapLinha = new LinkedHashMap<>();
		mapLinha.put("Todo", task);
		tabela.buscarLinha(mapLinha, 4, removeButton).click();
	}
	
	public void selecionarConcluida(String task) {
		Tabela tabela = new Tabela(driver, tasksTable);
		Map<String, String> mapLinha = new LinkedHashMap<>();
		mapLinha.put("Todo", task);
		tabela.buscarLinha(mapLinha, 0, concluidaCheck).click();
	}
}
