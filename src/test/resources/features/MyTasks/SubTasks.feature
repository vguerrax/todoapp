@my_tasks @subtask
Feature: Title of your feature

  Background: 
    Given eu tenha acessado a aplicação como usuário padrão
    And tenha acessado a página 'MyTasks'
    And exista tasks exibidas na lista

  @access_manage_subtasks_modal @automated
  Scenario: Acessar Modal de SubTasks
    When eu acionar a opção para gerenciar subtasks
    Then o sistema deve exibir o modal de subtasks com apresentar o ID e descrição da task desabilitados
    And o formulário para inclusão de subtask com os campos de descrição e data
    And o botão para adicionar subtasks
    And a lista de subtasks adcionadas na parte inferior

  @add_subtask @automated
  Scenario: Adicionar SubTask
    And eu acionar a opção para gerenciar subtasks
    When eu informar a descrição e a data da subtask
    And acionar a opção para adicionar subtask
    Then o sistema deve adicionar a subtask à lista de subtasks

  @add_subtask_required_fields @automated
  Scenario Outline: Adicionar SubTask - Campos Obrigatórios
    And eu acionar a opção para gerenciar subtasks
    When eu informar os dados da subtask sem informar o campo '<campo>'
    And acionar a opção para adicionar subtask
    Then o sistema não deve adicionar a subtask à lista de subtasks
    And deve informar que o campo '<campo>' é de preenchimento obrigatório

    Examples: 
      | campo     |
      | descrição |
      | data      |

  @add_subtask_description_length @automated
  Scenario: Adicionar SubTask - Tamanho Descrição
    And eu acionar a opção para gerenciar subtasks
    When eu preencher o formulário para inclusão de subtask com o campo de descrição com mais de 250 caracteres e a data
    And acionar a opção para adicionar subtask
    Then o sistema não deve adicionar a subtask à lista de subtasks
    And deve informar que a descrição deve possuir no máximo 250 caracteres

  @add_subtask_date_format @automated
  Scenario: Adicionar SubTask - Formato Data
    And eu acionar a opção para gerenciar subtasks
    When eu preencher o formulário para inclusão de subtask com o campo de descrição e a data '30/12/2019'
    And acionar a opção para adicionar subtask
    Then o sistema não deve adicionar a subtask à lista de subtasks
    And deve informar que o formato da data deve ser 'MM/dd/yyyy'

  @remove_subtask @automated
  Scenario: Remover SubTask
    And eu acionar a opção para gerenciar subtasks
    And exista subtasks para a task
    When eu acionar a opção para remover subtask
    Then o sistema deve remover a subtask da lista de subtasks
