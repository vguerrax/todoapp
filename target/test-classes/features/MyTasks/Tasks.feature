@my_tasks @task
Feature: Tasks

  Background: 
    Given eu tenha acessado a aplicação como usuário padrão

  @access_my_tasks @automated
  Scenario: Acessar My Tasks
    When acessar o link para a página 'My Tasks'
    Then o sistema deve apresentar a lista de tasks do usuário
    And apresentar a mensagem 'Hey <nome do usuario>, this is your todo list for today:'

  @add_task @automated
  Scenario Outline: Adicionar Task
    And tenha acessado a página 'MyTasks'
    When eu informar na descrição da task 'Task <data e hora atual>'
    And <acao>
    Then o sistema deve adicionar a task informada à lista de tasks

    Examples: 
      | acao                                 |
      | pressionar Enter                     |
      | acionar a opção para adcionar a Task |

  @add_task_length @automated
  Scenario Outline: Adicionar Task - Tamanho Descrição
    And tenha acessado a página 'MyTasks'
    When eu informar na descrição da task '<descricao>'
    And acionar a opção para adcionar a Task
    Then o sistema não deve permitir a inclusão da task
    And informar os tamanhos mínimo e máximo para a descrição da task

    Examples: 
      | descricao                          |
      | T                                  |
      | Ta                                 |
      | <texto com mais de 250 caracteres> |

  @remove_task @automated
  Scenario: Remover Task
    And tenha acessado a página 'MyTasks'
    And exista tasks exibidas na lista
    When eu aciono a opção para remover a task
    Then o sistema deve remover a task informada da lista de tasks